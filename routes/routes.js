const express = require('express');
const Model = require('../models/model');
const router = express.Router();

router.post('/insert', async (req, res) => {
    const data = new Model({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber
    })


    try {
        const dataToSave = await data.save();
        res.status(200).json({"status":true,
            "data":dataToSave})
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
})

router.get('/getAll', async (req, res) => {
    try {
        const data = await Model.find();
        res.status(200).json({status:true,data:data})
    }
    catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.get('/getOne', async (req, res) => {
    try {
        const data = await Model.findOne({email:req.body.email});
        if(data == null){
            res.status(200).json({message: "No result found"})
        }
        res.status(200).json(data)
    }
    catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.post('/update', async (req, res) => {
    try {
        const email = req.query.email;
        const updatedData = req.body;

        console.log(email,updatedData)
        const result = await Model.findOneAndUpdate({ email: email },updatedData, {upsert: true});
        res.send(result)
    }
    catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.delete('/delete', async (req, res) => {
    try {
        const email = req.body.email;
        const data = await Model.deleteOne({email: email})

        const result = await Model.find();
        res.status(200).json({status:true,data:result})
        }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
})

module.exports = router;